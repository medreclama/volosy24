import counters from './modules/counters';
import youtubeLoading from './modules/youtube-loading';
import catalogSlider from '../blocks/catalog-slider/catalog-slider';
import addMobileFilter from '../blocks/catalog-mobile-filter/catalog-mobile-filter';
import addFilterRange from '../blocks/catalog-filter-range/catalog-filter-range';
import addFilter from '../blocks/catalog-filter/catalog-filter';
import addTooltipPaletteMedia from '../blocks/palette/palette';
import Tooltip from '../blocks/tooltip/tooltip';
import scrollSocialsWidgets from '../blocks/socials-widget/socials-widget';
import Slider from '../blocks/slider/slider';
import catalogLoadingButtonInit from '../blocks/catalog/catalog';
import checkoutTable from '../blocks/checkout/checkout-table/checkout-table';
import accordionItem from '../blocks/accordion-item/accordion-item';
import video360Init from '../blocks/video-js/video-js';
import 'img-comparison-slider';

video360Init();

// import productNotHairSelectInit from '../blocks/product-main/product-main';

// productNotHairSelectInit();

$(document).ready(() => {
  $('.js-slider').flexslider({
    animation: 'slide',
    selector: '.js-slider__list > .js-slider__item',
    touch: true,
    prevText: '',
    nextText: '',
  });
  $.maskfn.definitions['~'] = '[+-]';
  $('body').delegate('input[type="tel"], input.phone', 'focus', () => {
    $(this).maskfn('+7 (999) 999-99-99');
  });
  $('.modal').fancybox();
});

const hairExtentionHeaderButtons = document.getElementsByClassName('hair-extention-header__button');
const hairExtentionSignSubheader = document.querySelector('.hair-extention-sign__header span');
const hairExtentionSignTypeInput = document.querySelector('.hair-extention-sign-form__input-type input');
const htmlElement = document.getElementsByTagName('html')[0];
const hairExtentionSignBottomButton = document.querySelector('.hair-extention-sign-bottom__button');

const hairExtentionButtons = Array.from(hairExtentionHeaderButtons);

if (hairExtentionSignBottomButton) {
  hairExtentionButtons.push(hairExtentionSignBottomButton);
}

hairExtentionButtons.forEach((item) => {
  item.addEventListener('click', (event) => {
    event.preventDefault();
    htmlElement.classList.add('no-scroll');
    const signWindowTypeText = event.target.dataset.name;
    hairExtentionSignSubheader.innerText = signWindowTypeText;
    if (signWindowTypeText !== 'наращивание') {
      hairExtentionSignTypeInput.setAttribute('value', signWindowTypeText);
    } else {
      hairExtentionSignTypeInput.setAttribute('value', 'тип не указан');
    }

    const signWindow = document.getElementById('hair-extention-sign');
    signWindow.classList.add('hair-extention-sign_show');
    const closeButton = signWindow.getElementsByClassName('hair-extention-sign__close')[0];
    closeButton.onclick = () => {
      signWindow.classList.remove('hair-extention-sign_show');
      htmlElement.classList.remove('no-scroll');
    };
  });
});

const mobileSwitchers = document.querySelectorAll('.mobile-nav__parent-label');

function mobileSwitch() {
  this.classList.toggle('mobile-nav__parent-label--active');
  this.nextElementSibling.classList.toggle('mobile-nav__sublist--display');
}

for (let i = 0; i < mobileSwitchers.length; i += 1) {
  mobileSwitchers[i].addEventListener('click', mobileSwitch);
}

const btnCart = document.getElementById('btnCart');

btnCart.addEventListener('click', (e) => {
  const target = e.currentTarget;
  e.preventDefault();
  target.classList.toggle('active');
});

const mrcSH = () => {
  const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
  const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
  mrcSHTitles.forEach((title) => {
    const itemObj = { name: title, bodies: [] };
    const itemName = title.dataset.mrcShTitle;
    const bodiesList = [];
    mrcSHBodies.forEach((body) => {
      const itemBody = body;
      const bodyName = itemBody.dataset.mrcShBody;
      if (itemName === bodyName) {
        bodiesList.push(itemBody);
        itemBody.style.display = 'none';
      }
    });
    if (bodiesList) {
      itemObj.bodies = bodiesList;
      itemObj.name.addEventListener('click', (e) => {
        e.preventDefault();
        bodiesList.forEach((item) => {
          const currentItem = item;
          currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
        });
      });
    }
  });
};

mrcSH();

// Добавить знак вопроса для 2. Выберите тип у товаров волос
const createHairTypeLink = () => {
  const chooseHairType = document.querySelector('.customfield_id_49');
  if (!chooseHairType) {
    const tabLabel = document.getElementById('product-choose-type');
    const tab = document.querySelector('.--chooseType');
    if (!tabLabel || !tab) return;
    tabLabel.remove();
    tab.remove();
  } else {
    const chooseHairTypeTitle = chooseHairType.querySelector('.product-properties__title');
    const link = document.createElement('a');
    link.href = '#product-choose-type';
    link.classList.add('product-properties__link');
    link.innerHTML = '?';
    chooseHairTypeTitle.append(link);
  }
};
createHairTypeLink();
document.addEventListener('ProductAjaxReplaced', () => {
  createHairTypeLink();
});

const tabsFunc = () => {
  const tabsList = Array.from(document.querySelectorAll('.tabs'));
  const tabActivate = (c, labels, tabs) => {
    if (!tabs[c].classList.contains('tab--active')) {
      labels.forEach((item, j) => {
        tabs[j].classList.remove('tab--active');
        labels[j].classList.remove('tabs__label--active');
        return item;
      });
      tabs[c].classList.add('tab--active');
      labels[c].classList.add('tabs__label--active');
    }
  };
  tabsList.forEach((block) => {
    const tl = Array.from(block.querySelectorAll('.tabs__label'));
    const tc = Array.from(block.querySelectorAll('.tab'));
    tl.forEach((label, i) => {
      const tab = tc[i];
      const tabLinks = label.id ? Array.from(document.querySelectorAll(`[href="#${label.id}"]`)) : undefined;
      if (+i === 0) {
        label.classList.add('tabs__label--active');
        tab.classList.add('tab--active');
      }
      label.addEventListener('click', () => tabActivate(i, tl, tc));
      if (tabLinks) {
        tabLinks.forEach((link) => {
          link.addEventListener('click', () => tabActivate(i, tl, tc));
        });
      }
      return label;
    });
    return block;
  });
};

tabsFunc();
document.addEventListener('ProductAjaxReplaced', () => {
  tabsFunc();
});

const addTooltipWholesalePalette = () => {
  const wholesalePalette = document.querySelector('.wholesale-properties__list');
  const tooltipWholesalePalette = new Tooltip(wholesalePalette);
  if (!wholesalePalette) return;
  tooltipWholesalePalette.createTooltipDiv('0.75rem', '0.2rem 0.4rem');
  tooltipWholesalePalette.init();
};
addTooltipWholesalePalette();

const productPropertiesMore = document.querySelector('.product-properties__more a');

if (productPropertiesMore) {
  productPropertiesMore.addEventListener('click', (e) => {
    e.preventDefault();
    e.target.style.display = 'none';
    const list = e.target.closest('.product-properties__list');
    list.classList.add('product-properties__list--active');
  });
}

const lengthSelect = () => {
  const select = document.querySelector('.product-properties__select--length');
  const imageBlock = document.querySelector('.product-photos__main');
  const image = document.querySelector('.product-photos__main a');
  const imageLength = document.querySelector('.product-photos__length');
  const ruler = document.querySelector('.product-photos__ruler');
  const rulerVal = document.querySelector('.product-photos__ruler-value');
  let mainHeight = null;
  if (imageLength) {
    imageLength.style.display = 'none';

    const setRuler = () => {
      const selected = select.options[select.selectedIndex].innerText;
      const s = +` ${selected.match(/(\d*)([\s\S]*)/)[1]}`;
      rulerVal.innerText = s;
      ruler.className = `product-photos__ruler product-photos__ruler--${s}`;
    };

    setRuler();

    const animateHeight = (from, to, target, duration) => {
      let start = null;
      let timer = null;
      const tick = (timestamp) => {
        start = start || timestamp;
        const elapsedTime = timestamp - start;
        const progress = elapsedTime / duration;
        const value = to > from ? from + (to - from) * progress : from - (from - to) * progress;
        if (progress >= 1) {
          target.style.height = `${to}px`; // eslint-disable-line
          target.style.height = ''; // eslint-disable-line
          return cancelAnimationFrame(timer);
        }
        target.style.height = `${value.toFixed(3)}px`; // eslint-disable-line
        timer = requestAnimationFrame(tick);
        return timer;
      };
      timer = requestAnimationFrame(tick);
    };
    select.addEventListener('mousedown', () => {
      mainHeight = imageBlock.offsetHeight;
      image.style.display = 'none';
      imageLength.style.display = 'block';
      ruler.style.display = 'block';
      const imgLHeight = imageBlock.offsetHeight;
      imageBlock.style.height = `${mainHeight}px`;
      animateHeight(mainHeight, imgLHeight, imageBlock, 200);
    });

    select.addEventListener('change', () => {
      setRuler();
    });

    select.addEventListener('blur', () => {
      const imgLHeight = imageBlock.offsetHeight;
      animateHeight(imgLHeight, mainHeight, imageBlock, 200);
      imageLength.style.display = 'none';
      ruler.style.display = 'none';
      image.style.display = 'block';
      imageLength.style.opacity = 1;
      ruler.style.opacity = 1;
    });
  }
};
lengthSelect();
document.addEventListener('ProductAjaxReplaced', () => {
  lengthSelect();
});

const counterHead = `
  <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://vk.com/js/api/openapi.js?169',t.onload=function(){VK.Retargeting.Init("VK-RTRG-1490268-aADDz"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-1490268-aADDz" style="position:fixed; left:-999px;" alt=""/></noscript>
  <!-- Google Tag 01.02.16 Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TZLZJT"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TZLZJT');</script>
  <!-- End Google Tag Manager -->

  <script type="text/javascript">
    var __cs = __cs || [];
    __cs.push(["setAccount", "Dn1fIZDTmCIAxevSg7eEp4080mNkTUrv"]);
    __cs.push(["setHost", "//server.comagic.ru/comagic"]);
  </script>

  <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>

  <script>
    // Widget.Twitter
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
  </script>
`;
const countersFooter = `
  <!--noindex-->
  <!-- VK Widget -->
  <script type="text/javascript">
    const container = document.getElementById('vk_groups');
    if (container) {
      const script = document.createElement('script');
      script.onload = () => VK.Widgets.Group("vk_groups", {mode: 3,'width': 'auto', height: 400}, 13278032);
      script.src = '//vk.com/js/api/openapi.js?151';
      document.body.append(script);
    }
  </script>
  <!--/noindex-->
`;

counters(counterHead, 'head');
counters(countersFooter);

youtubeLoading();

// eslint-disable-next-line no-undef
if (likely) likely.initiate();

// Таймер
const countdowns = document.querySelectorAll('.countdown');
countdowns.forEach((cd) => {
  const date = cd.dataset.date.split(',');
  // eslint-disable-next-line no-undef
  jQuery(cd).countdown({ timestamp: new Date(...date) });
});

catalogSlider();
addMobileFilter();
addFilterRange();
addFilter();
addTooltipPaletteMedia();
scrollSocialsWidgets();

const addTooltipProductPalette = () => {
  const productPalette = document.querySelector('.product-properties__list');
  const tooltipProductPalette = new Tooltip(productPalette);
  if (!productPalette) return;
  tooltipProductPalette.createTooltipDiv('0.75rem', '0.2rem 0.4rem');
  tooltipProductPalette.init();
};

// Слайдер в карточке товара с дополнительным фото товаров
const addProductPhotosSlider = () => {
  const productPhotosSlider = document.querySelector('.product-photos__slider .slider__slides');
  if (productPhotosSlider) {
    const productPhotosSliderInit = new Slider(productPhotosSlider, {
      breakpointsBase: 'container',
      spaceBetween: 12,
      breakpoints: {
        100: {
          slidesPerView: 1,
        },
        150: {
          slidesPerView: 2,
        },
        240: {
          slidesPerView: 3,
        },
        350: {
          slidesPerView: 4,
        },
        450: {
          slidesPerView: 5,
        },
      },
    });
    productPhotosSliderInit.init();
  }
};

const addProductVideoSlider = () => {
  const productVideoSlider = document.querySelector('.product-photos__video .slider__slides');
  if (productVideoSlider) {
    const productVideoSliderInit = new Slider(productVideoSlider, {
      breakpointsBase: 'container',
      spaceBetween: 12,
      breakpoints: {
        100: {
          slidesPerView: 1,
        },
        150: {
          slidesPerView: 2,
        },
        250: {
          slidesPerView: 3,
        },
      },
    });
    productVideoSliderInit.init();
  }
};

// Слайдер в карточке товара с фото в блоке "Как наши волосы выглядят в реальной жизни"
const addProductGallerySlider = () => {
  const productGallerySlider = document.querySelector('.product-gallery__slider .slider__slides');
  if (productGallerySlider) {
    const productGallerySliderInit = new Slider(productGallerySlider, {
      spaceBetween: 15,
      breakpoints: {
        800: {
          slidesPerView: 6,
        },
        480: {
          slidesPerView: 4,
        },
        320: {
          slidesPerView: 2,
        },
      },
    });
    productGallerySliderInit.init();
  }
};

// Слайдер в карточке товара с рекомендованными товарами
const addProductRecommendedSlider = () => {
  const productRecommendedSlider = document.querySelector('.product-recommended .slider__slides');
  if (productRecommendedSlider) {
    const productRecommendedSliderInit = new Slider(productRecommendedSlider, {
      watchSlidesProgress: true,
      breakpoints: {
        1200: {
          slidesPerView: 4,
        },
        1000: {
          slidesPerView: 4,
        },
        600: {
          slidesPerView: 3,
        },
        320: {
          slidesPerView: 2,
        },
      },
    });
    productRecommendedSliderInit.init();
  }
};

addProductRecommendedSlider();
addProductPhotosSlider();
addProductVideoSlider();
addProductGallerySlider();
addTooltipProductPalette();

// Ajax для слайдеров и тултипов в карточке товара
document.addEventListener('ProductAjaxReplaced', () => {
  addProductPhotosSlider();
  addProductVideoSlider();
  addProductGallerySlider();
  addTooltipProductPalette();
  addProductRecommendedSlider();
  document.querySelector('.tooltip').remove();
});

catalogLoadingButtonInit();
checkoutTable();
accordionItem();
