class Tooltip {
  #tooltip; // частная переменная (опред. вне конструктора)
  #container;

  constructor(container) { // задаем параметр контейнера, к которому нужно привязать тултип
    this.#container = container;
  }

  createTooltipDiv = (fontSize, padding) => {
    this.#tooltip = document.createElement('div');
    this.#tooltip.className = 'tooltip';
    this.#tooltip.hidden = true;
    this.#tooltip.style.pointerEvents = 'none';
    this.#tooltip.style.fontSize = fontSize;
    this.#tooltip.style.padding = padding;
    document.body.append(this.#tooltip);
  };

  #hideTooltip = () => {
    this.#tooltip.hidden = true;
    document.removeEventListener('scroll', this.#hideTooltip);
  };

  #showTooltip = (evt) => {
    const item = evt.target;
    if (!item.dataset.tooltip) return;
    this.#tooltip.innerHTML = item.dataset.tooltip;
    this.#tooltip.hidden = false;
    const itemRect = item.getBoundingClientRect();
    let x = 0;
    let y = 0;
    const tooltipHeight = parseInt(getComputedStyle(this.#tooltip).getPropertyValue('--border-width'), 10); // Получаем значение CSS переменной border-width, от которой зависит расположение тултипа по оси y
    x = itemRect.x;
    y = itemRect.y - this.#tooltip.offsetHeight - tooltipHeight;
    x = itemRect.x + item.offsetWidth / 2 - this.#tooltip.offsetWidth / 2;
    if (x < 0) x = 0;
    if (y < 0) {
      y = itemRect.y + item.offsetHeight + tooltipHeight;
      this.#tooltip.classList.add('tooltip--top');
    } else {
      this.#tooltip.classList.remove('tooltip--top');
    }
    this.#tooltip.style.left = `${x}px`;
    this.#tooltip.style.top = `${y}px`;
    document.addEventListener('scroll', this.#hideTooltip);
  };

  init = () => {
    this.#container.addEventListener('mouseout', this.#hideTooltip);
    this.#container.addEventListener('mouseover', this.#showTooltip);
  };

  destroy = () => {
    this.#container.removeEventListener('mouseout', this.#hideTooltip);
    this.#container.removeEventListener('mouseover', this.#showTooltip);
  };
}

export default Tooltip;
