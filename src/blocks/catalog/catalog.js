const CATALOG_ITEM_COUNT = 9;
const catalogLoadingButton = document.querySelector('.catalog__button');
let currentIndex = 0;

const catalogLoadingButtonInit = () => {
  if (!catalogLoadingButton) return;
  const catalog = document.querySelector('.catalog');
  const catalogItems = Array.from(catalog.querySelectorAll('.catalog__item'));
  catalogItems.forEach((item) => item.classList.add('catalog__item--hidden'));

  const handleShowMoreButtonClick = () => {
    const itemsToShow = catalogItems.slice(currentIndex, currentIndex + CATALOG_ITEM_COUNT);
    itemsToShow.forEach((item) => {
      item.classList.remove('catalog__item--hidden');
    });

    currentIndex += CATALOG_ITEM_COUNT;
    if (currentIndex >= catalogItems.length) catalogLoadingButton.style.display = 'none';
  };

  handleShowMoreButtonClick();

  if (catalogItems.length > CATALOG_ITEM_COUNT) {
    catalogLoadingButton.addEventListener('click', handleShowMoreButtonClick);
  }
};

export default catalogLoadingButtonInit;
