const checkoutTable = () => {
  const discountElement = document.querySelector('.checkout-table__total-discount strong');
  if (!discountElement) return;
  const discount = parseInt(discountElement.innerHTML.replace(/[^\d-]/g, ''), 10);
  discountElement.innerHTML = `${discount.toLocaleString('ru-RU')} руб.`;
};

export default checkoutTable;
