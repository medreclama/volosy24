function Scroll(delay) {
  // scroll event occurs if the page is refreshed when scrollY > 0
  // this suggests that the browser scrolls to previous (before refresh) position
  // eslint-disable-next-line no-restricted-globals
  this.initialY = scrollY;
  this.callbacks = {
    start: null,
    scroll: null,
    end: null,
  };
  this.timeout = {
    delay: delay || 150,
    handle: null,
  };
  this.handler = Scroll.handler.bind(this);
}

// eslint-disable-next-line func-names
Scroll.handler = function (e) {
  if (this.initialY === 0) {
    if (this.timeout.handle == null) {
      if (this.callbacks.start) {
        this.callbacks.start.call(this, e);
      }
    }
    if (this.timeout.handle !== null) {
      clearTimeout(this.timeout.handle);
    }
    if (this.callbacks.scroll) {
      this.callbacks.scroll.call(this, e);
    }
    const self = this;
    this.timeout.handle = setTimeout(() => {
      self.timeout.handle = null;
      if (self.callbacks.end) {
        self.callbacks.end.call(self, e);
      }
    }, this.timeout.delay);
  } else {
    this.initialY = 0;
  }
};

// eslint-disable-next-line func-names
Scroll.prototype.addEventListener = function (event, callback) {
  window.addEventListener('scroll', this.handler);
  if (Object.keys(this.callbacks).includes(event)) {
    this.callbacks[event] = callback;
  }
};

// eslint-disable-next-line func-names
Scroll.prototype.removeEventListener = function (event, callback) {
  window.removeEventListener('scroll', this.handler);
  if (Object.keys(this.callbacks).includes(event)) {
    this.callbacks[event] = callback;
  }
};

export default Scroll;
