import Tooltip from '../tooltip/tooltip';

const filter = document.querySelector('.catalog-filter');

// Скрыть-показать поля в фильтре
const addFilterShowHide = () => {
  filter.addEventListener('click', (evt) => {
    const clickedLabel = evt.target;
    if (clickedLabel.dataset.name === 'show-hide') {
      clickedLabel.nextElementSibling.classList.toggle('catalog-filter__show-hide--active');
    }
  });
};

const addFilter = () => {
  if (!filter) return;
  const filterColorList = filter.querySelector('.catalog-filter__color-list');
  const tooltipFilter = new Tooltip(filterColorList);
  tooltipFilter.createTooltipDiv();
  addFilterShowHide();
  tooltipFilter.init();
};

export default addFilter;
