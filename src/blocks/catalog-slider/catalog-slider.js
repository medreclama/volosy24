import Swiper, { Navigation } from 'swiper';

const catalogSlider = () => new Swiper('.catalog-slider__swiper', {
  modules: [Navigation],
  speed: 200,
  spaceBetween: 15,
  breakpoints: {
    320: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    480: {
      slidesPerView: 3,
    },
    800: {
      slidesPerView: 5,
    },
    1000: {
      slidesPerView: 6,
    },
  },

  navigation: {
    nextEl: '.catalog-slider__button--next',
    prevEl: '.catalog-slider__button--prev',
    disabledClass: 'catalog-slider__button--disabled',
  },
});

export default catalogSlider;
