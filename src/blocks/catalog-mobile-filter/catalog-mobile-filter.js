const mobileFilter = document.querySelector('.catalog-mobile-filter');

const addMobileFilter = () => {
  if (!mobileFilter) return;
  const filterOpenButton = mobileFilter.querySelector('.catalog-mobile-filter__button');
  filterOpenButton.closest('div').addEventListener('click', (evt) => {
    if (evt.target === filterOpenButton) {
      filterOpenButton.closest('div').classList.add('catalog-mobile-filter__block--active');
      document.body.style.overflow = 'hidden';
    } else if (evt.target.tagName === 'BUTTON' && evt.target !== filterOpenButton) {
      filterOpenButton.closest('div').classList.remove('catalog-mobile-filter__block--active');
      document.body.style.overflow = 'auto';
    }
  });
};

export default addMobileFilter;
