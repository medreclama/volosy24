import media from '../match-media/match-media';
import Scroll from '../scroll/scroll';

const scroll = new Scroll(500);

class ScrollSocialsWidget {
  constructor(container) {
    this.container = container;
  }

  #hideWidget = () => {
    this.container.classList.add('socials-widget--hidden');
  };

  #showWidget = () => {
    this.container.classList.remove('socials-widget--hidden');
  };

  init = () => {
    scroll.addEventListener('scroll', this.#hideWidget);
    scroll.addEventListener('end', this.#showWidget);
  };

  destroy = () => {
    scroll.removeEventListener('scroll', this.#hideWidget);
    scroll.removeEventListener('end', this.#showWidget);
  };
}

const widgetContainer = document.querySelector('.socials-widget');
const widget = new ScrollSocialsWidget(widgetContainer);

const scrollSocialsWidgets = () => {
  media.addChangeListener('max', 'm', scrollSocialsWidgets);
  if (media.max('m')) widget.init();
  else widget.destroy();
};

export default scrollSocialsWidgets;
