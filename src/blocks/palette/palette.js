import Tooltip from '../tooltip/tooltip';
import media from '../match-media/match-media';

const palettes = document.querySelectorAll('.palette');

const addTooltipPaletteMedia = () => {
  if (palettes.length === 0) return;
  palettes.forEach((palette) => {
    const tooltipPalette = new Tooltip(palette);
    tooltipPalette.createTooltipDiv('0.875rem', '0.2rem 0.5rem');
    media.addChangeListener('min', 'l', addTooltipPaletteMedia);
    if (media.min('l')) tooltipPalette.init();
    else tooltipPalette.destroy();
  });
};

export default addTooltipPaletteMedia;
