const productNotHairSelect = document.querySelector('.product-properties__list.product-field-display select');

// eslint-disable-next-line max-len
//  изменение кол-ва товара через дата атрибуты селекта для товаров-не волос (страница product-not-hair.html)
const productNotHairSelectInit = () => {
  if (!productNotHairSelect) return;
  const productAvailability = document.querySelector('.product-properties__availability');
  const productNotAvailability = document.querySelector('.product-properties__availability--not-available');
  const productAvailabilityMessage = productAvailability.querySelector('.curBalance_message');

  if (productNotHairSelect.options[0].dataset.quantity !== '') {
    productAvailabilityMessage.innerHTML = productNotHairSelect.options[0].dataset.quantity;
    productNotAvailability.style.display = 'none';
    productAvailability.style.display = 'flex';
  } else {
    productAvailability.style.display = 'none';
    productNotAvailability.style.display = 'flex';
  }

  productNotHairSelect.addEventListener('change', (evt) => {
    const option = evt.target.selectedOptions[0];
    const optionQuantity = option.dataset.quantity;
    if (optionQuantity !== '') {
      productAvailabilityMessage.innerHTML = optionQuantity;
      productNotAvailability.style.display = 'none';
      productAvailability.style.display = 'flex';
    } else {
      productAvailability.style.display = 'none';
      productNotAvailability.style.display = 'flex';
    }
  });
};

export default productNotHairSelectInit;
