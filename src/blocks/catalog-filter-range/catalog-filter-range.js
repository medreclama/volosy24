import noUiSlider from 'nouislider';

const rangeElement = document.querySelector('.catalog-filter-range');
const rangeFrom = document.getElementById('range-from');
const rangeTo = document.getElementById('range-to');

const rangeInputs = [rangeFrom, rangeTo];
const MAX_PRICE = rangeElement && Number(rangeElement.dataset.maxPrice);

// Инициализация noUiSlider
const initNoUiSlider = () => {
  noUiSlider.create(rangeElement, {
    range: {
      min: 0,
      max: MAX_PRICE,
    },
    connect: true,
    start: [rangeFrom.value ? rangeFrom.value : 0, rangeTo.value ? rangeTo.value : MAX_PRICE],
    step: 1,
    format: {
      to: (value) => value.toFixed(0),
      from: (value) => parseFloat(value),
    },
  });
};

// Изменение inputs при перетаскивании
const changeInputs = () => {
  rangeElement.noUiSlider.on('update', (values, handle) => {
    rangeInputs[handle].value = values[handle];
    if (values[handle] === '0' || values[handle] === `${MAX_PRICE}`) rangeInputs[handle].value = '';
  });
};

const addChangeEvent = () => {
  rangeElement.noUiSlider.on('change', (values, handle) => {
    rangeInputs[handle].dispatchEvent(new Event('change'));
  });
};

// Изменение range при вводе значения в input
const changeRange = () => {
  rangeInputs.forEach((input, handle) => {
    input.addEventListener('change', () => {
      if (input.value === '' && handle === 0) {
        rangeElement.noUiSlider.set([0, true]);
      } else if (input.value === '' && handle === 1) {
        rangeElement.noUiSlider.set([true, MAX_PRICE]);
      }
      rangeElement.noUiSlider.setHandle(handle, input.value);
    });
  });
};

const addFilterRange = () => {
  if (!rangeElement) return;
  initNoUiSlider();
  addChangeEvent();
  changeInputs();
  changeRange();
};

export default addFilterRange;
